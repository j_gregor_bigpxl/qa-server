const puppeteer = require("puppeteer");
const express = require("express");
var cors = require("cors");
const { scrollPageToBottom } = require("puppeteer-autoscroll-down");

const app = express();

app.use(cors());

app.get("/?", async (req, res) => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  page.setViewport({ width: 1920, height: 1080 });

  // Navigates to this page
  await page.goto(req.query.url); // URL is given by the "user" (your client-side application)

  // Grabs all of the H1 data, then loops through if there are multiple. Saves data to h1sObject
  h1Data = await page.evaluate(() => {
    const h1s = Array.from(document.querySelectorAll("h1"));
    var h1sObject = {
      totalH1s: h1s.length,
    };
    for (i = 0; i < h1s.length; i++) {
      h1sObject["h1#" + i] = h1s[i].textContent;
    }
    return h1sObject;
  });

  // Scrolls to bottom of page
  await scrollPageToBottom(page, { size: 300, delay: 150 });

  // Take fullpage screenshot
  await page.screenshot({
    path: "./screenshot.jpeg", // Save the screenshot in current directory
    type: "jpeg",
    fullPage: true, // take a fullpage screenshot
  });

  res.send(h1Data);
  await browser.close();
});

app.listen(80, function () {
  console.log("CORS-enabled web server listening on port 80");
});

// TODO:
// - Fullpage screenshots of every page (13" or 15")
//   - Put screenshot somewhere
